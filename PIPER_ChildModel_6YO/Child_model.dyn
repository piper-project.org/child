$# Copyright (C) 2017 UCBL,Ifsttar (trunk, extremities; geometry except foot)
$# Copyright (C) 2017 KTH (head and neck, flesh, assembly) 
$# Copyright (C) 2017 TU Berlin (pelvis, lower extremities)
$# Foot geometry: derived from a segmentation by CEESAR (License: CC-BY-4.0)
$# 
$# This file is part of the PIPER Child Human Body Model (HBM). (see doc)
$# Version 0.99.0
$# The PIPER Child HBM is free software: you can redistribute it and/or modify
$# it under the terms of the GNU General Public License as published by the 
$# Free Software Foundation, either version 3 of the License, or (at your 
$# any later version.  The PIPER Child HBM  is distributed in the hope that it
$# will be useful but WITHOUT ANY WARRANTY; without even the implied warranty of
$# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
$# Public License for more details. You should have received a copy of the GNU 
$# General Public License along with the PIPER Child HBM. If not, see 
$# <http://www.gnu.org/licenses/>. 
$# According to the term of the GPL v3 article 7, the Parties hereby agree to
$# include to GPL v3 the following additional terms:
$#
$# - Any public release of any scientific claims or data etc. that were either
$# supported or generated by the Model or a modification thereof, whether in 
$# whole or in part, shall be made available to the public under the same 
$# license as the PIPER Child HBM. This additional License term applies to any
$# modification of the Model.
$# - Models representing man-made objects, which can be manufactured, or that 
$# do not represent part of the human anatomy are not considered as 
$# modifications or derivatives of the Model and are therefore not subjected 
$# to the obligation referred to above to make such objects publicly available.
$# - in no event, unless required by applicable law, shall any copyright holder,
$# or any other third party who modifies and/or conveys the model as permitted 
$# in accordance with this legal license, be liable to any other Party or third 
$# party for any indirect or direct damages, including but not limited to death,
$# bodily injury and property damages (public or private), even if such holder 
$# or other party has been advised of the possibility of such damages. Any use 
$# of the model is at your own risk.
$# 
$# This work has received funding from the European Union Seventh Framework 
$# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
$# Contributors include:
$# * Philippe Beillas, Anicet Le Ruyet, Marie-Christine Chevalier, Xingjia
$#   Yin, Jerome Collot [UBCL,Ifsttar]
$# * Chiara Giordano, Victor S. Alvarez, Xiaogai Li, Svein Kleiven [KTH]
$# * Stefan Kirscht, Ahmed Saeed, William Goede [TU Berlin]
$#
$# Work by UCBL,Ifsttar on preliminary versions of a child model has received
$# funding from the French Ministry of Industry (ProEtech project, 2011-2014)
$# and the EU FP7 GA 218564 [CASPER project]. Contributors: Philippe Beillas,
$# Anurag Soni, Fabien Berthet, Xavier Bourdin, Edison Zapata)
$#
*KEYWORD
$
*INCLUDE
main.k
$
*INCLUDE
Child_model_Piper_entities.key
*INCLUDE
CTRLPT_PIPER_Source_NODES.k
*INCLUDE
landmarks_ISB.k
*INCLUDE
PIPER_landmark_namedmetadata.k
*INCLUDE
PIPER_landmark_namedmetadataCL.k
*INCLUDE
PIPER_kriging_decomposition_namedmetadata.k
$
$
*TITLE
PIPER Child Model
$
$
*END