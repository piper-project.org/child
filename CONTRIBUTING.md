# contribution Guide

## For the PIPER Child models

Validation cases are provided for many impact conditions. If you would like
to contribute something to the model, please explain the proposition and re-run
all validation cases to check the global effect on the model response.

Please be careful about rounding errors of node coordinates or renumbering.
Only propose update of parts that need to be changed.

## For the validation cases
Please organize your new contribution like for others i.e.
* link to the model (relative include). Do not include the model in the setup
* use the same license (CC-BY-4.0)
* provide the python script for automatic post-processing

