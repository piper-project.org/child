$# This file is needed to perform the Loyd (2011) validation case.
$# Author: KTH (2017)
$# License: Creative Commons Attribution 4.0 International License.
$# (https://creativecommons.org/licenses/by/4.0/)
$#
$# This work has received funding from the European Union Seventh Framework 
$# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
$# Contributors are C. Giordano.
$# See the PIPER Child documentation for more info.
$#
*KEYWORD
*CONTROL_BULK_VISCOSITY
$#      q1        q2      type     btype
  1.500000  0.060000         1         0
*CONTROL_CONTACT
$#  slsfac    rwpnal    islchk    shlthk    penopt    thkchg     orien    enmass
     0.000     0.000         1         2         0         0         2         0
$#  usrstr    usrfrc     nsbcs    interm     xpene     ssthk      ecdt   tiedprj
         0         0         0         0 10.000000         0         0         1
$#   sfric     dfric       edc       vfc        th     th_sf    pen_sf
     0.000     0.000     0.000     0.000     0.000     0.000     0.000
$#  ignore    frceng   skiprwg    outseg   spotstp   spotdel   spothin
         0         0         0         0         0         0     0.000
$#    isym    nserod    rwgaps    rwgdth     rwksf      icov    swradf    ithoff
         0         0         0     0.000  1.000000         0     0.000         0
$#  shledg    pstiff    ithcnt    tdcnof     ftall    unused    shltrw
         0         0         0         0         0         0     0.000
*CONTROL_CPU
$#  cputim
     0.000
*CONTROL_DAMPING
$#  nrcyck     drtol    drfctr    drterm    tssfdr    irelal     edttl    idrflg
         0     0.000     0.000     0.000  0.800000         0     0.000         0
*CONTROL_ENERGY
$#    hgen      rwen    slnten     rylen
         2         2         2         2
*CONTROL_HOURGLASS
$#     ihq        qh
         3  0.100000
*CONTROL_OUTPUT
$#   npopt    neecho    nrefup    iaccop     opifs    ipnint    ikedit    iflush
         0         0         0         0     0.000         0         0         0
$#   iprtf    ierode     tet10    msgmax    ipcurv
         0         0         2        50         0
*CONTROL_SHELL
$#  wrpang     esort     irnxx    istupd    theory       bwc     miter      proj
 45.000000         1        -1         0         2         2         1         0
$# rotascl    intgrd    lamsht    cstyp6    tshell    nfail1    nfail4   psnfail
  1.000000         0         0         1         0         0         0         0
$# psstupd    irquad     cntco    itsflg    irquad
         0         0         0         0         2
*CONTROL_SOLID
$#   esort   fmatrix   niptets    swlocl    psfail
         1         1         4         2         0
$#   pm1     pm2     pm3     pm4     pm5     pm6     pm7     pm8     pm9    pm10
       0       0       0       0       0       0       0       0       0       0
*CONTROL_TERMINATION
$#  endtim    endcyc     dtmin    endeng    endmas
   6.00000         0     0.000     0.000     0.000
*CONTROL_TIMESTEP
$#  dtinit    tssfac      isdo    tslimt     dt2ms      lctm     erode     ms1st
     0.000  0.850000         0     0.000     0.000         0         0         0
$#  dt2msf   dt2mslc     imscl    unused    unused     rmscl
     0.000         0         0         0         0     0.000
*DATABASE_ABSTAT
$#      dt    binary      lcur     ioopt
  0.001000         0         0         1
*DATABASE_DEFORC
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_ELOUT
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_GLSTAT
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_MATSUM
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_NCFORC
$#      dt    binary      lcur     ioopt
  0.200000         0         0         1
*DATABASE_NODOUT
$#      dt    binary      lcur     ioopt      dthf     binhf
 0.1000000         0         0         1     0.000         0
*DATABASE_RBDOUT
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_RCFORC
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_RWFORC
$#      dt    binary      lcur     ioopt
  0.001000         0         0         1
*DATABASE_SBTOUT
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_SLEOUT
$#      dt    binary      lcur     ioopt
 0.1000000         0         0         1
*DATABASE_BINARY_D3PLOT
$#      dt      lcdt      beam     npltc    psetid
 0.1000000         0         0         0         0
$#   ioopt
         0
*DATABASE_BINARY_D3THDT
$#      dt      lcdt      beam     npltc    psetid
 0.0500000         0         0         0         0
*INCLUDE
plate4.k
*DEFINE_TRANSFORMATION_TITLE
Body model position
$:   label
         2
$:  option        a1        a2        a3
POINT              3         0         0         0
POINT              4         0         1         0
ROTATE             3         4       -25
$                  X         Y         Z
TRANSL       175.565     0.000    25.000 
*INCLUDE_TRANSFORM
../../../PIPER_ChildModel_6YO/main_Head_Skull.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
         1         1         1  1.000000         1         0
$#  tranid
         2
*INCLUDE_TRANSFORM
../../../PIPER_ChildModel_6YO/main_Head_Brain.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
         1         1         1  1.000000         1         0
$#  tranid
         2
*INCLUDE_TRANSFORM
../../../PIPER_ChildModel_6YO/main_FleshSkin_HeadNeck.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
         1         1         1  1.000000         1         0
$#  tranid
         2
*INCLUDE_TRANSFORM
../../../PIPER_ChildModel_6YO/main_FleshSkin.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
         1         1         1  1.000000         1         0
$#  tranid
         2		 
*INCLUDE_TRANSFORM
../../../PIPER_ChildModel_6YO/main_Flesh_HeadNeck_InterfaceNodes.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
         1         1         1  1.000000         1         0
$#  tranid
         2
*BOUNDARY_SPC_SET
$#    nsid       cid      dofx      dofy      dofz     dofrx     dofry     dofrz
  60152326         0         1         1         1         0         0         0
*SET_PART_LIST_TITLE
Scalp
$#     sid       da1       da2       da3       da4    solver
  60152325     0.000     0.000     0.000     0.000MECH
$#    pid1      pid2      pid3      pid4      pid5      pid6      pid7      pid8
      1000      1001         0         0         0         0         0         0
*CONTACT_AUTOMATIC_SURFACE_TO_SURFACE
$#     cid                                                                 title
$#    ssid      msid     sstyp     mstyp    sboxid    mboxid       spr       mpr
  60152325  60152327         2         3         0         0         0         0
$#      fs        fd        dc        vc       vdc    penchk        bt        dt
  0.400000  0.400000     0.000     0.000 20.000000         0     0.0001.0000E+20
$#     sfs       sfm       sst       mst      sfst      sfmt       fsf       vsf
  20.00000  1.000000     0.000     0.000  1.000000  1.000000  1.000000  1.000000
*INITIAL_VELOCITY_GENERATION
$#nsid/pid      styp     omega        vx        vy        vz     ivatn      icid
     10001         1     0.000   -1.7040     0.000     0.000         0         0
$#      xc        yc        zc        nx        ny        nz     phase    iridid
     0.000     0.000     0.000     0.000     0.000     0.000         0         0
*SET_PART_LIST_GENERATE
$#     sid       da1       da2       da3       da4    solver
     10001     0.000     0.000     0.000     0.000MECH
$#   b1beg     b1end     b2beg     b2end     b3beg     b3end     b4beg     b4end
      1000      1712         0         0         0         0         0         0 
*END