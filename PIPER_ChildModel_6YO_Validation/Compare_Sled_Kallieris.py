# Description: See the PIPER framework or model documentation for more info.
# Author: KTH
# Version: 1.0.0
# License: Creative Commons Attribution 4.0 International License.
# (https://creativecommons.org/licenses/by/4.0/)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# Contributors include Chiara Giordano, Victor S. Alvarez, Xiaogai Li, Svein
# Kleiven [KTH]
# 
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 16:05:48 2017

@author: vicsa
"""

import numpy as np
import matplotlib.pyplot as plt
import subprocess
import os
import scipy.io as spio

try: import tkinter.filedialog as filedialog           # Py3
except ImportError: import tkFileDialog as filedialog  # Py2


ftypes = [('exe files', '*.exe')]


try:
    mat = spio.loadmat('PrePostpath.mat', squeeze_me=True)
    PrePostpath = mat['PrePostpath'] # array
    if not PrePostpath:
        f = filedialog.askopenfile(mode='r', filetypes=ftypes)
        spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})
except (IOError, KeyError):
    f = filedialog.askopenfile(mode='r', filetypes=ftypes)
    spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})

MainPath = os.getcwd()


#====== Kallieris 1976 Sled ===================================================
Kallieris_ForceL = np.loadtxt(MainPath+'\Full_Body\Sleds\Kallieris_EXP_Data\Kallieris_ExpL.force',skiprows=0)
Kallieris_ForceR = np.loadtxt(MainPath+'\Full_Body\Sleds\Kallieris_EXP_Data\Kallieris_ExpR.force',skiprows=0)


f = plt.figure(figsize=(15,5))
ax12 = f.add_subplot(121)
ax12.plot(Kallieris_ForceL[:,0],Kallieris_ForceL[:,1],'-k',lw=2)
ax12.plot(Kallieris_ForceR[:,0],Kallieris_ForceR[:,1],'-b',lw=2)


mat = spio.loadmat(MainPath+'\Full_Body\Sleds\Kallieris_EXP_Data\Kallieris_XZ.mat', squeeze_me=True)
Data = mat['Data'] # array
Kallieris_HeadXZ = Data
ax12 = f.add_subplot(122)
ax12.plot(Kallieris_HeadXZ[:,0],Kallieris_HeadXZ[:,1],'-k',lw=2)

#------- Extract Simulation data ----------------
Cfile6 = open('Full_Body\Sleds\Extract_Data_Kallieris.cfile','w');

init = 'binaski init;'+'\n'+'binaski load "'
SimPathK = '\Full_Body\Sleds\Kallieris_1976'
Binload = '\\binout0000"'+'\n'
Binload2 = '\\binout0000;'+'\n'
BinSwitch = 'binaski fileswitch ';
LoadToAscii = 'binaski saveas ascii'+'\n'+'binaski write nodout'+'\n'+'binaski write sbtout'+'\n'+'binaski write rbdout'+'\n';
AsciiOpenF1 = 'ascii sbtout open "';
AsciiOpenF2 = '\\sbtout" 0'+'\n';
PlotF = 'ascii sbtout plot 1 SB-65199/SB-65200/'+'\n'   ;
SaveF = 'xyplot 1 savefile xypair "BeltForce.txt" 1 all'+'\n';
AsciiOpenD1 = 'ascii nodout open "';
AsciiOpenD2 = '\\nodout" 0'+'\n';
PlotD = 'ascii nodout plot 1/3 1706609/1703675'+'\n';
SaveD = 'xyplot 1 savefile xypair "HeadDispl.txt" 1 all'+'\n';
AsciiOpenRb1 = 'ascii rbdout open "';
AsciiOpenRb2 = '\\rbdout" 0'+'\n';
PlotD2 = 'ascii rbdout plot 4/6 7'+'\n';
SaveD2 = 'xyplot 1 savefile xypair "SledDispl.txt" 1 all'+'\n';


Cfile6.write(init + MainPath + SimPathK + Binload + BinSwitch + MainPath + SimPathK +
Binload2 + LoadToAscii + AsciiOpenF1 + MainPath + SimPathK + AsciiOpenF2 +
PlotF + SaveF +  
AsciiOpenD1 + MainPath + SimPathK + AsciiOpenD2 + PlotD + SaveD +  
AsciiOpenRb1 + MainPath + SimPathK + AsciiOpenRb2 + PlotD2 + SaveD2 +  
'quit')

Cfile6.close()

#subprocess.call([PrePostpath,MainPath+'\Full_Body\Sleds\Extract_Data_Kallieris.cfile', '-nographics'])

Kallieris_Force = open(MainPath+SimPathK+'\BeltForce.txt','r');

time = []
Force = []
endplot = []
row = 0
for line in Kallieris_Force.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        Force.append(float(xy[1]))


ShieldBeltR = (Force[0:endplot[1]-2])
Time1 = (time[0:endplot[1]-2])
ShieldBeltL = (Force[endplot[1]-2:len(Force)])
Time2 = (time[endplot[1]-2:len(time)])

ax12 = f.add_subplot(121)
ax12.plot([t-90 for t in Time1],ShieldBeltR,'-g',lw=2)
ax12.plot([t-90 for t in Time1],ShieldBeltL,'-r',lw=2)
ax12.set_title('Shield Belt Force')
ax12.set_xlabel('Time [ms]')
ax12.set_ylabel('Force [KN]')
ax12.legend(['Experiment-LeftBelt','Experiment-RightBelt','PIPER 6YO Right','PIPER 6YO Left'],loc='best',prop={'size':9})


Kallieris_HeadDispl = open(MainPath+SimPathK+'\HeadDispl.txt','r');
time = []
Displ = []
endplot = []
row = 0
for line in Kallieris_HeadDispl.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        Displ.append(float(xy[1]))

Head_XDispl_L = (Displ[0:endplot[1]-2])
Time1 = (time[0:endplot[1]-2])
Head_XDispl_R = (Displ[endplot[1]-2:endplot[2]-3])
Time2 = (time[endplot[1]-2:endplot[2]-3])
Head_ZDispl_L = (Displ[endplot[2]-3:endplot[3]-4])
Time3 = (time[endplot[2]-3:endplot[3]-4])
Head_ZDispl_R = (Displ[endplot[3]-4:len(Displ)])
Time4 = (time[endplot[3]-4:len(Displ)])

Head_XDispl = [sum(x)/2 for x in zip(Head_XDispl_L, Head_XDispl_R)]
Head_ZDispl = [sum(x)/2 for x in zip(Head_ZDispl_L, Head_ZDispl_R)]


Kallieris_SledDispl = open(MainPath+SimPathK+'\SledDispl.txt','r');
time = []
Displ = []
endplot = []
row = 0
for line in Kallieris_SledDispl.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        Displ.append(float(xy[1]))

Slead_XDispl = (Displ[0:endplot[1]-2])
Time1 = (time[0:endplot[1]-2])
Slead_ZDispl = (Displ[endplot[1]-2:len(Displ)])
Time2 = (time[endplot[1]-2:len(time)])


t1_0 = 90
t1_near = min(time, key=lambda x:abs(x-t1_0))
t1 = Time2.index(t1_near)


z0 = Head_ZDispl[t1]
Head_ZDispl = [z-z0 for z in Head_ZDispl]

Head_XDisplRel = [b-a for a, b in zip(Slead_XDispl[t1:len(Slead_XDispl)], Head_XDispl[t1:len(Head_XDispl)])]
Head_ZDisplRel = [b-a for a, b in zip(Slead_ZDispl[t1:len(Slead_ZDispl)], Head_ZDispl[t1:len(Head_ZDispl)])]


ax12 = f.add_subplot(122)
ax12.plot(Head_XDisplRel,Head_ZDisplRel,'-r',lw=2)
ax12.set_title('Head Excursion')
ax12.set_xlabel('x-displ [mm]')
ax12.set_ylabel('z-displ [mm]')
ax12.legend(['Experiment','PIPER 6YO Left'],loc='best',prop={'size':9})
ax12.set_ylim([-350, 0])
ax12.set_xlim([0, 700])

f.savefig('Kallieris_Sled_Comparison.png')


