# Description: See the PIPER framework or model documentation for more info.
# Author: KTH
# Version: 1.0.0
# License: Creative Commons Attribution 4.0 International License.
# (https://creativecommons.org/licenses/by/4.0/)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# Contributors include Chiara Giordano, Victor S. Alvarez, Xiaogai Li, Svein
# Kleiven [KTH]
# 
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 13:26:26 2017

@author: vicsa
"""

import numpy as np
import matplotlib.pyplot as plt
import subprocess
import os
import scipy.io as spio

try: import tkinter.filedialog as filedialog           # Py3
except ImportError: import tkFileDialog as filedialog  # Py2

ftypes = [('exe files', '*.exe')]

try:
    mat = spio.loadmat('PrePostpath.mat', squeeze_me=True)
    PrePostpath = mat['PrePostpath'] # array
    if not PrePostpath:
        f = filedialog.askopenfilename(initialdir = "C:\\" ,title = 'Browse and choose LS-PrePost executable',filetypes=ftypes)
        spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})
except (IOError, KeyError):
    f = filedialog.askopenfile(initialdir = "C:\\" ,title = 'Browse and choose LS-PrePost executable',filetypes=ftypes)
    spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})

MainPath = os.getcwd()

#====== Ouyang et al. 2005 ====================================================
#------ Bending ---------------------------------
mat = spio.loadmat('Neck\Ouyang_Bending.mat', squeeze_me=True,)
data = mat['Data'] # array

f = plt.figure(figsize=(15,12))
ax1 = f.add_subplot(221)
ax1.plot(data[:,0],data[:,1]) 
ax1.plot(data[:,18],data[:,19])
ax1.plot(data[:,4],data[:,5])
ax1.plot(data[:,6],data[:,7])
ax1.plot(data[:,8],data[:,9])
ax1.plot(data[:,10],data[:,11])
ax1.plot(data[:,12],data[:,13])
ax1.plot(data[:,14],data[:,15])
ax1.plot(data[:,16],data[:,17])
ax1.plot(data[:,2],data[:,3])
ax1.set_title('C2 Rotation [Ouyang et al. (2005)]')
ax1.set_xlabel('Moment [Nm]')
ax1.set_ylabel('Rotation [deg]')

ax2 = f.add_subplot(222)
ax2.plot(data[:,20],data[:,21])
ax2.plot(data[:,38],data[:,39])
ax2.plot(data[:,24],data[:,25])
ax2.plot(data[:,26],data[:,27])
ax2.plot(data[:,28],data[:,29])
ax2.plot(data[:,30],data[:,31])
ax2.plot(data[:,32],data[:,33])
ax2.plot(data[:,34],data[:,35])
ax2.plot(data[:,36],data[:,37])
ax2.plot(data[:,22],data[:,23])
ax2.set_title('T1 Rotation [Ouyang et al. (2005)]')
ax2.set_xlabel('Moment [Nm]')
ax2.set_ylabel('Rotation [deg]')

#------- Extract Simulation data ----------------
Cfile2 = open('Neck\Extract_Data_OuyangBending.cfile','w');

init = 'binaski init;'+'\n'+'binaski load "'
SimPath1 = '\Neck\Ouyang_2005_Bending_08Nm'
SimPath2 = '\Neck\Ouyang_2005_Bending_16Nm'
SimPath3 = '\Neck\Ouyang_2005_Bending_24Nm'
SimPath4 = '\Neck\Ouyang_2005_Bending_Neg08Nm'
SimPath5 = '\Neck\Ouyang_2005_Bending_Neg16Nm'
SimPath6 = '\Neck\Ouyang_2005_Bending_Neg24Nm'
Binload = '\\binout0000"'+'\n'
Binload2 = '\\binout0000;'+'\n'
BinSwitch = 'binaski fileswitch '
LoadToAscii = 'binaski saveas ascii'+'\n'+'binaski write rbdout'+'\n'
AsciiOpen1 = 'ascii rbdout open "'
AsciiOpen2 = '\\rbdout" 0'+'\n'
PlotF = 'ascii rbdout addplot 8 2102/2108'+'\n'
Unload = 'ascii rbdout uload'+'\n'
SaveF = 'xyplot 1 savefile xypair "C2_T2_yRotations_6YO.txt"'

Cfile2.write(init+MainPath+SimPath1+Binload+BinSwitch+MainPath+SimPath1+Binload2+
LoadToAscii+AsciiOpen1+MainPath+SimPath1+AsciiOpen2+PlotF+Unload+'\n'+init+MainPath+
SimPath2+Binload+BinSwitch+MainPath+SimPath2+Binload2+LoadToAscii+AsciiOpen1+
MainPath+SimPath2+AsciiOpen2+PlotF+Unload+'\n'+init+MainPath+SimPath3+Binload+
BinSwitch+MainPath+SimPath3+Binload2+LoadToAscii+AsciiOpen1+MainPath+SimPath3+
AsciiOpen2+PlotF+Unload+'\n'+init+MainPath+SimPath4+Binload+BinSwitch+MainPath+
SimPath4+Binload2+LoadToAscii+AsciiOpen1+MainPath+SimPath4+AsciiOpen2+PlotF+Unload+
'\n'+init+MainPath+SimPath5+Binload+BinSwitch+MainPath+SimPath5+Binload2+LoadToAscii+
AsciiOpen1+MainPath+SimPath5+AsciiOpen2+PlotF+Unload+'\n'+init+MainPath+SimPath6+
Binload+BinSwitch+MainPath+SimPath6+Binload2+LoadToAscii+AsciiOpen1+MainPath+
SimPath6+AsciiOpen2+PlotF+Unload+SaveF+'\nquit')

Cfile2.close()

#subprocess.call([PrePostpath,MainPath+'\Neck\Extract_Data_OuyangBending.cfile', '-nographics'])

Ouyang_Bending = open(MainPath+SimPath6+'\C2_T2_yRotations_6YO.txt','r');
time = []
angle = []
endplot = []
row = 0
for line in Ouyang_Bending.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        angle.append(float(xy[1]))

degrees08_C1 = np.mean(angle[endplot[1]-52:endplot[1]-2])*180/np.pi
degrees08_T1 = np.mean(angle[endplot[2]-54:endplot[2]-3])*180/np.pi
degrees16_C1 = np.mean(angle[endplot[3]-55:endplot[3]-4])*180/np.pi
degrees16_T1 = np.mean(angle[endplot[4]-56:endplot[4]-5])*180/np.pi
degrees24_C1 = np.mean(angle[endplot[5]-57:endplot[5]-6])*180/np.pi
degrees24_T1 = np.mean(angle[endplot[6]-58:endplot[6]-8])*180/np.pi
degreesNeg08_C1 = np.mean(angle[endplot[7]-59:endplot[7]-9])*180/np.pi
degreesNeg08_T1 = np.mean(angle[endplot[8]-60:endplot[8]-10])*180/np.pi
degreesNeg16_C1 = np.mean(angle[endplot[9]-61:endplot[9]-11])*180/np.pi
degreesNeg16_T1 = np.mean(angle[endplot[10]-62:endplot[10]-12])*180/np.pi
degreesNeg24_C1 = np.mean(angle[endplot[11]-63:endplot[11]-13])*180/np.pi
degreesNeg24_T1 = np.mean(angle[len(angle)-50:len(angle)])*180/np.pi
moment = [-2.4, -1.6, -0.8, 0, 0.8, 1.6, 2.4]

ax1.plot(moment,[-degrees24_C1,-degrees16_C1,-degrees08_C1,0,-degreesNeg08_C1,-degreesNeg16_C1,-degreesNeg24_C1],'k-o',lw=2)
ax1.legend(['2 YO','2.5 YO','3 YO','3 YO','4 YO','5 YO','6 YO','6 YO','7.5 YO','12 YO','PIPER 6YO'],loc='best',prop={'size':10})

ax2.plot(moment,[-degrees24_T1,-degrees16_T1,-degrees08_T1,0,-degreesNeg08_T1,-degreesNeg16_T1,-degreesNeg24_T1],'k-o',lw=2)
ax2.legend(['2 YO','2.5 YO','3 YO','3 YO','4 YO','5 YO','6 YO','6 YO','7.5 YO','12 YO','PIPER 6YO'],loc='best',prop={'size':10})

#------ Tension ---------------------------------
mat = spio.loadmat('Neck\Ouyang_Tension.mat', squeeze_me=True,)
data = mat['data'] # array

ax3 = f.add_subplot(223)
ax3.plot(data[0][:,0],data[0][:,1])
ax3.plot(data[1][:,0],data[1][:,1])
ax3.plot(data[2][:,0],data[2][:,1])
ax3.plot(data[3][:,0],data[3][:,1])
ax3.plot(data[4][:,0],data[4][:,1])
ax3.plot(data[5][:,0],data[5][:,1])
ax3.plot(data[6][:,0],data[6][:,1])
ax3.plot(data[7][:,0],data[7][:,1])
ax3.plot(data[8][:,0],data[8][:,1])
ax3.set_title('Whole Spine Tension [Ouyang et al. (2005)]')
ax3.set_xlabel('Displacement [mm]')
ax3.set_ylabel('Force [N]')
ax3.legend(['2 YO','2.5 YO','3 YO','3 YO','4 YO','6 YO','6 YO','7.5 YO','12 YO'])

#------- Extract Simulation data ----------------
Cfile = open('Neck\Extract_Data_OuyangTension.cfile','w');

init = 'binaski init;'+'\n'+'binaski load "'
SimPath = '\Neck\Ouyang_2005_Tension_5mmSec'
Binload = '\\binout0000"'+'\n'
Binload2 = '\\binout0000;'+'\n'
BinSwitch = 'binaski fileswitch ';
LoadToAscii = 'binaski saveas ascii'+'\n'+'binaski write spcforc'+'\n';
AsciiOpen1 = 'ascii spcforc open "';
AsciiOpen2 = '\spcforc" 0'+'\n';
PlotF = 'ascii spcforc plot 3 total/all'+'\n';
SaveF1 = 'xyplot 1 savefile xypair "';
SaveF2 = '\Force" 1 all'+'\n';
LoadDisp = 'binaski loadblock /rbdout'+'\n';
PlotD1 = 'binaski plot "';
PlotD2 = '\\binout0000" rbdout 1 1 global 1100 global_dz ;'+'\n';
SaveD1 = 'xyplot 1 savefile xypair "';
SaveD2 = '\Displ" 1 all'+'\n';
CrossPlot = 'cross Force~1 Displ~1 1000'+'\n';
SaveCross = 'xyplot 1 savefile xypair "Force_Displ_Tension_6YO.txt" 1 all'+'\n';

Cfile.write(init + MainPath + SimPath + Binload + BinSwitch + MainPath + SimPath +
Binload2 + LoadToAscii + AsciiOpen1 + MainPath + SimPath + AsciiOpen2 + PlotF +
SaveF1 + MainPath + SimPath + SaveF2 + LoadDisp + PlotD1 + MainPath + SimPath +
PlotD2 + SaveD1 + MainPath + SimPath + SaveD2 + CrossPlot + SaveCross + 'quit')

Cfile.close()

#subprocess.call([PrePostpath,MainPath+'\Neck\Extract_Data_OuyangTension.cfile', '-nographics'])

Ouyang_Tension=np.loadtxt(MainPath+SimPath+'\Force_Displ_Tension_6YO.txt',skiprows=1)
ax3.plot(Ouyang_Tension[:,0],-Ouyang_Tension[:,1]*1e3,'-k',lw=2)
ax3.legend(['2 YO','2.5 YO','3 YO','3 YO','4 YO','6 YO','6 YO','7.5 YO','12 YO','PIPER 6YO'],prop={'size':10})
ax3.set_ylim([0, 1000])

#====== Luck et al. 2008 ======================================================
mat = spio.loadmat('Neck\Luck_Tension.mat', squeeze_me=True)
Data = mat['Data'] # array

Luck_22MN = Data[0]
Luck_9YR = Data[1]
Luck_14YR = Data[2]

ax4 = f.add_subplot(224)
ax4.plot(Luck_22MN[:,0],Luck_22MN[:,1],'--')
ax4.plot(Luck_9YR[:,0],Luck_9YR[:,1],'--')
ax4.plot(Luck_14YR[:,0],Luck_14YR[:,1],'--')
ax4.set_title('Whole Spine Tension [Luck et al. 2008]')
ax4.set_xlabel('Displacement [mm]')
ax4.set_ylabel('Force [N]')

#------- Extract Simulation data ----------------
Cfile3 = open('Neck\Extract_Data_LuckTension.cfile','w');

init = 'binaski init;'+'\n'+'binaski load "'
SimPath = '\Neck\Luck_2008_Tension_Force'
Binload = '\\binout0000"'+'\n'
Binload2 = '\\binout0000;'+'\n'
BinSwitch = 'binaski fileswitch '
LoadToAscii = 'binaski saveas ascii'+'\n'+'binaski write spcforc'+'\n'+'binaski write rbdout'+'\n'
AsciiOpen1 = 'ascii spcforc open "'
AsciiOpen2 = '\spcforc" 0'+'\n'
PlotF = 'ascii spcforc plot 3 total/all'+'\n'
SaveF1 = 'xyplot 1 savefile xypair "'
SaveF2 = '\Force" 1 all'+'\n'
AsciiOpenR1 = 'ascii rbdout open "'
AsciiOpenR2 = '\\rbdout" 0\n'
PlotD1 = 'ascii rbdout plot 6 2108\n'

SaveD1 = 'xyplot 1 savefile xypair "'
SaveD2 = '\Displ" 1 all'+'\n'
CrossPlot = 'cross Force~1 Displ~1 1000'+'\n'
SaveCross = 'xyplot 1 savefile xypair "Force_Displ_Tension_Luck_6YO.txt" 1 all'+'\n'

Cfile3.write(init+MainPath+SimPath+Binload+BinSwitch+MainPath+SimPath+Binload2+
LoadToAscii+AsciiOpen1+MainPath+SimPath+AsciiOpen2+PlotF+SaveF1+MainPath+SimPath+
SaveF2+AsciiOpenR1+MainPath+SimPath+AsciiOpenR2+PlotD1+SaveD1+MainPath+SimPath+SaveD2+
CrossPlot+SaveCross+'quit')

Cfile3.close()

#subprocess.call([PrePostpath,MainPath+'\Neck\Extract_Data_LuckTension.cfile', '-nographics'])

Luck_data=np.loadtxt(MainPath+SimPath+'\Force_Displ_Tension_Luck_6YO.txt',skiprows=1)

ax4.plot(-Luck_data[:,0],Luck_data[:,1]*1e3,'-k')
ax4.legend(['Luck et al. 2008 22 Month','Luck et al. 2008 9 Year','Luck et al. 2008 14 Year','PIPER 6YO Model'],loc='best',prop={'size':12})
ax4.set_xlim([0, 6])
ax4.set_ylim([0, 350])

f.savefig('Neck_Spine_Comparison.png')


















