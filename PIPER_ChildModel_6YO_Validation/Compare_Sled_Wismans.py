# Description: See the PIPER framework or model documentation for more info.
# Author: KTH
# Version: 1.0.0
# License: Creative Commons Attribution 4.0 International License.
# (https://creativecommons.org/licenses/by/4.0/)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# Contributors include Chiara Giordano, Victor S. Alvarez, Xiaogai Li, Svein
# Kleiven [KTH]
# 
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 07 18:03:44 2017

@author: vicsa
"""

import numpy as np
import matplotlib.pyplot as plt
import subprocess
import os
import scipy.io as spio

try: import tkinter.filedialog as filedialog           # Py3
except ImportError: import tkFileDialog as filedialog  # Py2


ftypes = [('exe files', '*.exe')]


try:
    mat = spio.loadmat('PrePostpath.mat', squeeze_me=True)
    PrePostpath = mat['PrePostpath'] # array
    if not PrePostpath:
        f = filedialog.askopenfile(mode='r', filetypes=ftypes)
        spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})
except (IOError, KeyError):
    f = filedialog.askopenfile(mode='r', filetypes=ftypes)
    spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})

MainPath = os.getcwd()


#====== Wismans 1979 Sled ====================================================
Wismans_Head_Acc = np.loadtxt(MainPath+'\Full_Body\Sleds\Wismans_EXP_Data\Wismans_Exp.head.acc',skiprows=0)
Wismans_LapForce = np.loadtxt(MainPath+'\Full_Body\Sleds\Wismans_EXP_Data\Wismans_Exp.lap.force',skiprows=0)
Wismans_BSForce = np.loadtxt(MainPath+'\Full_Body\Sleds\Wismans_EXP_Data\Wismans_Exp.BS.force',skiprows=0)
Wismans_DiagForce = np.loadtxt(MainPath+'\Full_Body\Sleds\Wismans_EXP_Data\Wismans_Exp.diag.force',skiprows=0)


f = plt.figure(figsize=(10,10))
ax8 = f.add_subplot(221)
ax8.plot(Wismans_LapForce[:,0],Wismans_LapForce[:,1],'-k',lw=2)
ax9 = f.add_subplot(222)
ax9.plot(Wismans_DiagForce[:,0],Wismans_DiagForce[:,1],'-k',lw=2)
ax10 = f.add_subplot(223)
ax10.plot(Wismans_BSForce[:,0],Wismans_BSForce[:,1],'-k',lw=2)
ax11 = f.add_subplot(224)
ax11.plot(Wismans_Head_Acc[:,0],Wismans_Head_Acc[:,1],'-k',lw=2)


#------- Extract Simulation data ----------------
Cfile5 = open('Full_Body\Sleds\Extract_Data_Wismans.cfile','w');

init = 'binaski init;'+'\n'+'binaski load "'
SimPathA = '\Full_Body\Sleds\Wismans_1979'
Binload = '\\binout0000"'+'\n'
Binload2 = '\\binout0000;'+'\n'
BinSwitch = 'binaski fileswitch ';
LoadToAscii = 'binaski saveas ascii'+'\n'+'binaski write nodout'+'\n'+'binaski write sbtout'+'\n';
AsciiOpenF1 = 'ascii sbtout open "';
AsciiOpenF2 = '\\sbtout" 0'+'\n';
PlotF = 'ascii sbtout plot 1 SB-80000344/SB-80000345/SB-80000355/SB-80000352/SB-80000348/'+'\n'   ;
SaveF = 'xyplot 1 savefile xypair "BeltForce.txt" 1 all'+'\n';
AsciiOpenA1 = 'ascii nodout open "';
AsciiOpenA2 = '\\nodout" 0'+'\n';
PlotA = 'ascii nodout plot 12 1102785/1100938/1102308'+'\n';
Filter = 'xyplot 1 filter sae 300.00 msec 0'+'\n';
SaveA = 'xyplot 1 savefile xypair "HeadAcc.txt" 1 all'+'\n';


Cfile5.write(init + MainPath + SimPathA + Binload + BinSwitch + MainPath + SimPathA +
Binload2 + LoadToAscii + AsciiOpenF1 + MainPath + SimPathA + AsciiOpenF2 +
PlotF + SaveF +  
AsciiOpenA1 + MainPath + SimPathA + AsciiOpenA2 + PlotA + Filter + SaveA +
'quit')

Cfile5.close()

#subprocess.call([PrePostpath,MainPath+'\Full_Body\Sleds\Extract_Data_Wismans.cfile', '-nographics'])


Wismans_Force = open(MainPath+SimPathA+'\BeltForce.txt','r');

time = []
Force = []
endplot = []
row = 0
for line in Wismans_Force.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        Force.append(float(xy[1]))


LapBelt1 = (Force[0:endplot[1]-2])
Time1 = (time[0:endplot[1]-2])
LapBelt2 = (Force[endplot[1]-2:endplot[2]-3])
Time2 = (time[endplot[1]-2:endplot[2]-3])
SeatBeltL = (Force[endplot[2]-3:endplot[3]-4])
Time3 = (time[endplot[2]-3:endplot[3]-4])
SeatBeltR = (Force[endplot[3]-4:endplot[4]-5])
Time4 = (time[endplot[3]-4:endplot[4]-5])
MainForce = (Force[endplot[4]-5:len(Force)])
Time5 = (time[endplot[4]-5:len(time)])

LapBelt = [sum(x)/2 for x in zip(LapBelt1, LapBelt2)]
SeatBelt = [sum(x)/2 for x in zip(SeatBeltL, SeatBeltR)]
Time1 = [x/1 for x in Time1]
Time2 = [x/1 for x in Time2]


ax8.plot(Time1,LapBelt,'-g',lw=2)
ax8.set_title('Lap Belt Force')
ax8.set_xlabel('Time [ms]')
ax8.set_ylabel('Force [KN]')
ax8.legend(['Experiment','PIPER 6YO'],loc='best',prop={'size':12})
ax9.plot(Time2,SeatBelt,'-g',lw=2)
ax9.set_xlabel('Time [ms]')
ax9.set_ylabel('Force [KN]')
ax9.set_title('Seat Belt Force')
ax10.plot(Time3,MainForce,'-g',lw=2)
ax10.set_title('Anchor Force')
ax10.set_xlabel('Time [ms]')
ax10.set_ylabel('Force [KN]')


Wismans_Acc = open(MainPath+SimPathA+'\HeadAcc.txt','r');

time = []
Acc = []
endplot = []
row = 0
for line in Wismans_Acc.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        Acc.append(float(xy[1]))


HeadAcc1 = (Acc[0:endplot[1]-2])
Time1 = (time[0:endplot[1]-2])
HeadAcc2 = (Acc[endplot[1]-2:endplot[2]-3])
Time2 = (time[endplot[1]-2:endplot[2]-3])
HeadAcc3 = (Acc[endplot[2]-3:len(Acc)])
Time3 = (time[endplot[2]-3:len(time)])

Head_Acc = [(a1+a2+a3)/3/9.81*1000 for a1, a2, a3 in zip(HeadAcc1,HeadAcc2,HeadAcc3)]


ax11.plot(Time1,Head_Acc,'-g',lw=2)
ax11.set_title('Head Acc')
ax11.set_xlabel('Time [ms]')
ax11.set_ylabel('Acc [G]')
ax11.legend(['Experiment','PIPER 6YO'],loc='best',prop={'size':12})
ax11.set_ylim([0, 130])
ax11.set_xlim([0, 100])

f.savefig('Wismans_Sled_Comparison.png')

