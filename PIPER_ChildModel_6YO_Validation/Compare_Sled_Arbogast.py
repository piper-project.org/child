# Description: See the PIPER framework or model documentation for more info.
# Author: KTH
# Version: 1.0.0
# License: Creative Commons Attribution 4.0 International License.
# (https://creativecommons.org/licenses/by/4.0/)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# Contributors include Chiara Giordano, Victor S. Alvarez, Xiaogai Li, Svein
# Kleiven [KTH]
# 
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 10:36:44 2017

@author: vicsa
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 02 16:37:45 2017

@author: vicsa
"""

import numpy as np
import matplotlib.pyplot as plt
import subprocess
import os
import scipy.io as spio

try: import tkinter.filedialog as filedialog           # Py3
except ImportError: import tkFileDialog as filedialog  # Py2

ftypes = [('exe files', '*.exe')]

try:
    mat = spio.loadmat('PrePostpath.mat', squeeze_me=True)
    PrePostpath = mat['PrePostpath'] # array
    if not PrePostpath:
        f = filedialog.askopenfile(mode='r', filetypes=ftypes)
        spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})
except (IOError, KeyError):
    f = filedialog.askopenfile(mode='r', filetypes=ftypes)
    spio.savemat('PrePostpath.mat',mdict={'PrePostpath': str(f.name)})

MainPath = os.getcwd()

#====== Arbogast 2009 Sled ====================================================
Arbogast_Top_diag = np.loadtxt(MainPath+'\Full_Body\Sleds\Arbogast_EXP_Data\CHOP_Exp.diag.force',skiprows=0)
Arbogast_Top_lap = np.loadtxt(MainPath+'\Full_Body\Sleds\Arbogast_EXP_Data\CHOP_Exp.lap.force',skiprows=0)

mat = spio.loadmat('Full_Body\Sleds\Arbogast_XZ.mat', squeeze_me=True,)
Data = mat['Data'] # array

Arbogast_Top_Head = Data[0]
Arbogast_Top_c4 = Data[1]
Arbogast_Top_T1 = Data[2]
Arbogast_Top_T4 = Data[3]
Arbogast_Top_T8 = Data[4]
Arbogast_Top_LIC = Data[5]

f = plt.figure(figsize=(18,12))
ax5 = f.add_subplot(221)
ax5.plot(Arbogast_Top_Head[:,0],Arbogast_Top_Head[:,1],'-.k',lw=2)
ax5.plot(Arbogast_Top_c4[:,0],Arbogast_Top_c4[:,1],'-.r',lw=2)
ax5.plot(Arbogast_Top_T1[:,0],Arbogast_Top_T1[:,1],'-.m',lw=2)
ax5.plot(Arbogast_Top_T4[:,0],Arbogast_Top_T4[:,1],'-.c',lw=2)
ax5.plot(Arbogast_Top_T8[:,0],Arbogast_Top_T8[:,1],'-.g',lw=2)
ax5.plot(Arbogast_Top_LIC[:,0],Arbogast_Top_LIC[:,1],'-.y',lw=2)
ax5.set_ylim([0, 1.2])


ax6 = f.add_subplot(223)
ax6.plot(Arbogast_Top_diag[:,0],Arbogast_Top_diag[:,1],'-k',lw=2)
ax6.set_ylim([0, 250])
ax7 = f.add_subplot(224)
ax7.plot(Arbogast_Top_lap[:,0],Arbogast_Top_lap[:,1],'-k',lw=2)
ax7.set_ylim([0, 250])

#------- Extract Simulation data ----------------
Cfile4 = open('Full_Body\Sleds\Extract_Data_Arbogast.cfile','w');

init = 'binaski init;'+'\n'+'binaski load "'
SimPathA = '\Full_Body\Sleds\Arbogast_2009'
Binload = '\\binout0000"'+'\n'
Binload2 = '\\binout0000;'+'\n'
BinSwitch = 'binaski fileswitch ';
LoadToAscii = 'binaski saveas ascii'+'\n'+'binaski write nodout'+'\n'+'binaski write sbtout'+'\n';
AsciiOpen1 = 'ascii nodout open "';
AsciiOpen2 = '\\nodout" 0'+'\n';
PlotD = 'ascii nodout plot 28/30 40/473/1703193/2140919/2182154/4113995/4114004/8201871'+'\n';
SaveXZ = 'xyplot 1 savefile xypair "XZ_Coordinates.txt" 1 all'+'\n';
AsciiOpenF1 = 'ascii sbtout open "';
AsciiOpenF2 = '\\sbtout" 0'+'\n';
PlotF = 'ascii sbtout plot 1 SB-42/SB-44/SB-25/SB-26'+'\n';
SaveF = 'xyplot 1 savefile xypair "BeltForce.txt" 1 all'+'\n';


Cfile4.write(init + MainPath + SimPathA + Binload + BinSwitch + MainPath + SimPathA +
Binload2 + LoadToAscii + AsciiOpen1 + MainPath + SimPathA + AsciiOpen2 + PlotD +
SaveXZ + AsciiOpenF1 + MainPath + SimPathA + AsciiOpenF2 +
PlotF + SaveF +  'quit')

Cfile4.close()

#subprocess.call([PrePostpath,MainPath+'\Full_Body\Sleds\Extract_Data_Arbogast.cfile', '-nographics'])

Arbogast_XZ = open(MainPath+SimPathA+'\XZ_Coordinates.txt','r');

time = []
CC = []
endplot = []
row = 0
for line in Arbogast_XZ.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        CC.append(float(xy[1]))


Seat1_X = (CC[0:endplot[1]-2])
Seat2_X = (CC[endplot[1]-2:endplot[2]-3])
TopHead_X = (CC[endplot[2]-3:endplot[3]-4])
C4_X = (CC[endplot[3]-4:endplot[4]-5])
T1_X = (CC[endplot[4]-5:endplot[5]-6])
T4_X = (CC[endplot[5]-6:endplot[6]-7])
T8_X = (CC[endplot[6]-7:endplot[7]-8])
Iliac_X = (CC[endplot[7]-8:endplot[8]-9])
Seat1_Z = (CC[endplot[8]-9:endplot[9]-10])
Seat2_Z = (CC[endplot[9]-10:endplot[10]-11])
TopHead_Z = (CC[endplot[10]-11:endplot[11]-12])
C4_Z = (CC[endplot[11]-12:endplot[12]-13])
T1_Z = (CC[endplot[12]-13:endplot[13]-14])
T4_Z = (CC[endplot[13]-14:endplot[14]-15])
T8_Z = (CC[endplot[14]-15:endplot[15]-16])
Iliac_z = (CC[endplot[15]-16:len(CC)])

t1_0 = 90
tEnd_0 = 420

t1_near = min(time, key=lambda x:abs(x-t1_0))
tEnd_near = min(time, key=lambda x:abs(x-tEnd_0))


t1 = time.index(t1_near)
tEnd = time.index(tEnd_near)

TopHead_Z = [i-j for i, j in zip(TopHead_Z, Seat2_Z)]
C4_Z = [i-j for i, j in zip(C4_Z, Seat2_Z)]
T1_Z = [i-j for i, j in zip(T1_Z, Seat2_Z)]
T4_Z = [i-j for i, j in zip(T4_Z, Seat2_Z)]
T8_Z = [i-j for i, j in zip(T8_Z, Seat2_Z)]
Iliac_Z = [i-j for i, j in zip(Iliac_z, Seat2_Z)]

TopHead_X = [i-j for i, j in zip(TopHead_X, Seat2_X)]
C4_X = [i-j for i, j in zip(C4_X, Seat2_X)]
T1_X = [i-j for i, j in zip(T1_X, Seat2_X)]
T4_X = [i-j for i, j in zip(T4_X, Seat2_X)]
T8_X = [i-j for i, j in zip(T8_X, Seat2_X)]
Iliac_X = [i-j for i, j in zip(Iliac_X, Seat2_X)]

ax5.plot([x/TopHead_Z[t1]+0.04 for x in TopHead_X[t1:tEnd]],[y/TopHead_Z[t1] for y in TopHead_Z[t1:tEnd]],'-k',lw=1.5)
ax5.plot([x/TopHead_Z[t1]-0.01 for x in C4_X[t1:tEnd]],[y/TopHead_Z[t1]+0.028 for y in C4_Z[t1:tEnd]],'-r',lw=2)
ax5.plot([x/TopHead_Z[t1] for x in T1_X[t1:tEnd]],[y/TopHead_Z[t1]+0.045 for y in T1_Z[t1:tEnd]],'-m',lw=2)
ax5.plot([x/TopHead_Z[t1]-0.02 for x in T4_X[t1:tEnd]],[y/TopHead_Z[t1]+0.028 for y in T4_Z[t1:tEnd]],'-c',lw=2)
ax5.plot([x/TopHead_Z[t1]-0.03 for x in T8_X[t1:tEnd]],[y/TopHead_Z[t1]+0.008 for y in T8_Z[t1:tEnd]],'-g',lw=2)
ax5.plot([x/TopHead_Z[t1]-0.05 for x in Iliac_X[t1:tEnd]],[y/TopHead_Z[t1]+0.045 for y in Iliac_Z[t1:tEnd]],'-y',lw=2)
ax5.set_title('Trajectories')
ax5.set_xlabel('Normalized X []')
ax5.set_ylabel('Normalized Z [KN]')
ax5.legend(['Experiment \nHead','C4','T1','T4','T8','Left iliac crest','Piper 6YO'],loc='best',prop={'size':10})


Arbogast_Force = open(MainPath+SimPathA+'\BeltForce.txt','r');

time = []
Force = []
endplot = []
row = 0
for line in Arbogast_Force.readlines():
    xy = line.split()    
    row = row+1
    if len(xy)==1:        
        endplot.append(row)
    if len(xy)==2:         
        time.append(float(xy[0]))
        Force.append(float(xy[1]))


DiaBelt1 = (Force[0:endplot[1]-2])
Time1 = (time[0:endplot[1]-2])
DiaBelt2 = (Force[endplot[1]-2:endplot[2]-3])
Time2 = (time[endplot[1]-2:endplot[2]-3])
LapBelt1 = (Force[endplot[2]-3:endplot[3]-4])
Time3 = (time[endplot[2]-3:endplot[3]-4])
LapBelt2 = (Force[endplot[3]-4:len(Force)])
Time4 = (time[endplot[3]-4:len(time)])

DiaBelt = [sum(x)*1000/2 for x in zip(DiaBelt1, DiaBelt2)]
LapBelt = [sum(x)*1000/2 for x in zip(LapBelt1, LapBelt2)]
Time1 = [(x+50)/1000 for x in Time1]
Time2 = [(x+50)/1000 for x in Time2]


ax6.plot(Time1,DiaBelt,'-g',lw=2)
ax6.set_ylim([0, 500])
ax6.set_title('Shoulder Belt Force')
ax6.set_xlabel('Time [ms]')
ax6.set_ylabel('Force [KN]')
ax6.legend(['Experiment','PIPER 6YO'],loc='best',prop={'size':12})
ax7.plot(Time2,LapBelt,'-g',lw=2)
ax7.set_ylim([0, 500])
ax7.set_title('Lap Belt Force')
ax7.set_xlabel('Time [ms]')
ax7.set_ylabel('Force [KN]')
ax7.legend(['Experiment','PIPER 6YO'],loc='best',prop={'size':12})


f.savefig('Arbogast_Sled_Comparison.png')
