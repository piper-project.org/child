This folder contains the input needed to perform the Chamouard et al. (ESC1996) validation case.
The files in this folder (Author: UCBL-Ifsttar) are licensed under a Creative Commons 
Attribution 4.0 International License. (https://creativecommons.org/licenses/by/4.0/)

This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
Contributors are Anicet Le Ruyet.

See the PIPER Child documentation for more info.