# -*- coding: utf-8 -*-
# This file runs all cfile files with LS-Prepost that could find in the subdirectories, outputs are processed 
# and are plotted along experimental curves.
# Arguments: none. You just have to put the path of LSPP and to enable (post_treat_LSPP=1) or disable (post_treat_LSPP=0)
# to specify if binout have to be processed
# Author: UCBL-Ifsttar (2017)
# License: Creative Commons Attribution 4.0 International License.
# (https://creativecommons.org/licenses/by/4.0/)
#
# This work has received funding from the European Union Seventh Framework 
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# Contributors are Philippe Beillas, Anicet Le Ruyet and M-C. Chevalier.
# See the PIPER Child documentation for more info.
#
import numpy as np
import matplotlib.pyplot as plt
import os
import re

post_treat_LSPP=0 #set 1 if binout have to be processed 

numeric_const_pattern = r""" [-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )  (?: [Ee] [+-]? \d+ ) ? """
rx = re.compile(numeric_const_pattern, re.VERBOSE)
  

# lsPath is a string with the location of Ls-Prepost on the computer
lsPath='C:\\LSTC\\LS-PrePost\\4.3-x64\\lsprepost4.3_x64.exe '
# resPath is a string with the location of the result folder "PIPER_Child_6YO_Validation" on the computer
resPath=os.path.join(os.getcwd())+'\\'
#resPath='U:\\Deliverable\\'
Path = lsPath+resPath

###############################################################################
###################### >>6 y-0 child<< ########################################
###############################################################################

########################Abdomen################################################

# EEVC
path_temp='PIPER_Child_6YO_Validation\\Abdomen\\EEVC\\'
file=open(resPath+path_temp+'pp_EEVC.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "' + resPath+path_temp + 'binout0000"\n')
file.write('binaski fileswitch "' + resPath+path_temp + 'binout0000\n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 35006317 x_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n1_x-displacement" 1 all\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" rcforc 1 1 S-1 x_force ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'rcforc" 1 all\n')
file.write('quit')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_EEVC.cfile -nographics')
n_Sim_EEVC = np.loadtxt(resPath+path_temp+'n1_x-displacement',skiprows=1)
force_Sim_EEVC_abdo_6y0 = np.loadtxt(resPath+path_temp+'rcforc',skiprows=1)
disp_Sim_EEVC_abdo_6y0=-n_Sim_EEVC;
EXP_EEVC_disp_abdo_6y0 = np.loadtxt(resPath+path_temp+'EEVC_Exp.displ')
EXP_EEVC_force_abdo_6y0 = np.loadtxt(resPath+path_temp+'EEVC_Exp.force')

# Flexion_part572
path_temp='PIPER_Child_6YO_Validation\\Abdomen\\Flexion_part572\\'
file=open(resPath+path_temp+'pp_flexion.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "' + resPath+path_temp + 'binout0000"\n')
file.write('binaski fileswitch ' + resPath+path_temp + 'binout0000;\n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 3 35006258 x_coordinate ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n1_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 3 35006258 y_coordinate ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n1_y-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 3 35006258 z_coordinate ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n1_z-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 3 35006263 x_coordinate ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n2_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 3 35006263 y_coordinate ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n2_y-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 3 35006263 z_coordinate ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n2_z-displacement" 1 all\n')
file.write('binaski loadblock /sbtout\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" sbtout 1 1 61 belt_force ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'belt_force" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_flexion.cfile -nographics')
n1_x_Sim_flexion = np.loadtxt(resPath+path_temp+'n1_x-displacement',skiprows=1)
n1_z_Sim_flexion = np.loadtxt(resPath+path_temp+'n1_z-displacement',skiprows=1)
n2_x_Sim_flexion = np.loadtxt(resPath+path_temp+'n2_x-displacement',skiprows=1)
n2_z_Sim_flexion = np.loadtxt(resPath+path_temp+'n2_z-displacement',skiprows=1)
teta_flexion=180/np.pi*np.arctan(np.absolute((n2_x_Sim_flexion[:,1]-n1_x_Sim_flexion[:,1])/(n2_z_Sim_flexion[:,1]-n1_z_Sim_flexion[:,1])))
force_Sim_flexion_tmp = np.loadtxt(resPath+path_temp+'belt_force',skiprows=1)
force_Sim_flexion=[]
force_Sim_flexion=[1000*force_Sim_flexion_tmp[i,1] for i in range(0,len(force_Sim_flexion_tmp)-2,10)]
                   
#Kent_Lower
path_temp='PIPER_Child_6YO_Validation\\Abdomen\\Kent_lower\\'
file=open(resPath+path_temp+'pp_Kent_Lower.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "' + resPath+path_temp + 'binout0000"\n')
file.write('binaski fileswitch ' + resPath+path_temp + 'binout0000;\n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 35006312 x_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n1_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 35006330 x_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n2_x-displacement" 1 all\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" rcforc 1 1 S-1 x_force ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'belt_force" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Kent_Lower.cfile -nographics')

n1_Sim_Kent_Lower = np.loadtxt(resPath+path_temp+'n1_x-displacement',skiprows=1)
n2_Sim_Kent_Lower = np.loadtxt(resPath+path_temp+'n2_x-displacement',skiprows=1)
n_Sim_Kent_Lower=[-0.5*(n1_Sim_Kent_Lower[i,1]+n2_Sim_Kent_Lower[i,1]) for i in range(len(n1_Sim_Kent_Lower[:,1]))]
force_Sim_Kent_Lower = np.loadtxt(resPath+path_temp+'belt_force',skiprows=1)
disp_Sim_Kent_Lower=n_Sim_Kent_Lower;
EXP_Kent_Lower10_disp = np.loadtxt(resPath+path_temp+'Lower.Kent10.displ')
EXP_Kent_Lower10_force = np.loadtxt(resPath+path_temp+'Lower.Kent10.force')
EXP_Kent_Lower25_disp = np.loadtxt(resPath+path_temp+'Lower.Kent25.displ')
EXP_Kent_Lower25_force = np.loadtxt(resPath+path_temp+'Lower.Kent25.force')

########################LEX####################################################

# Chamouard thigh
path_temp='PIPER_Child_6YO_Validation\\Lower_extremities\\Chamouard_thigh\\'
file=open(resPath+path_temp+'pp_Cham_thigh.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "' + resPath+path_temp + 'binout0000"\n')
file.write('binaski fileswitch ' + resPath+path_temp + 'binout0000;\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" rcforc 1 1 S-18 z_force ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'rcforc" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995296 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n1_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995237 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n2_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995178 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n3_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995119 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n4_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995060 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n5_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995118 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n6_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995177 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n7_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995236 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n8_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995238 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n9_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995179 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n10_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995120 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n11_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995180 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n12_x-displacement" 1 all\n')
file.write('binaski plot "' + resPath+path_temp + 'binout0000" nodout 1 1 99995176 z_displacement ;\n')
file.write('xyplot 1 savefile xypair "' + resPath+path_temp + 'n13_x-displacement" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Cham_thigh.cfile -nographics')
n_Sim_Cham_Thigh_tmp={};n_Sim_Cham_Thigh=[]
for i in range(13):
    n_Sim_Cham_Thigh_tmp[i] = np.loadtxt(resPath+path_temp+'n'+str(i+1)+'_x-displacement',skiprows=1)
for i in range(len(n_Sim_Cham_Thigh_tmp[0][:,0])):
    n_Sim_Cham_Thigh.append(-np.mean([n_Sim_Cham_Thigh_tmp[j][i][1] for j in range(13)]))
force_Sim_Cham_Thigh = np.loadtxt(resPath+path_temp+'rcforc',skiprows=1)
disp_Sim_Cham_Thigh=n_Sim_Cham_Thigh;
EXP_Cham_Thigh_inf = np.loadtxt(resPath+path_temp+'Cham_Thigh_Lim_Inf.txt')
EXP_Cham_Thigh_sup = np.loadtxt(resPath+path_temp+'Cham_Thigh_Lim_Sup.txt')

########################Pelvis#################################################

# Ouyang 2003
path_temp='PIPER_Child_6YO_Validation\\Pelvis\\Ouyang-2003\\'
file=open(resPath+path_temp+'pp_Ouy2003.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch "binout0000";\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 S-1 y_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('genselect node add node 99908809/0\n')
file.write('binaski plot "binout0000" nodout 1 1 99908809 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout1" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Ouy2003.cfile -nographics')
force_sim_Ouy6y0_pel=np.loadtxt(resPath+path_temp+ 'force',skiprows=1)
nod1_Ouy6y0_pel=np.loadtxt(resPath+path_temp+ 'nodout1',skiprows=1)
force_Ouy6y0_pel=np.loadtxt(resPath+path_temp+ 'Ouyang_Subj8.txt')


############################Shoulder###########################################

# EEVC Shoulder 6yo
path_temp='PIPER_Child_6YO_Validation\\Shoulder\\EEVC-Q2008\\'
file=open(resPath+path_temp+'pp_EEVC-Q2008.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 S-1 y_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('genselect node add node 4116934/0\n')
file.write('binaski plot "binout0000" nodout 1 1 4116934 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout1" 1 all\n')
file.write('genselect node add node 20001548/0\n')
file.write('binaski plot "binout0000" nodout 1 1 20001548 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout2" 1 all\n')
file.write('quit\n')

if post_treat_LSPP:
    os.system(Path+path_temp+'pp_EEVC-Q2008.cfile -nographics')
force_sim_EEVC_shoul_6y0=np.loadtxt(resPath+path_temp+'force',skiprows=1)
nod1_EEVC_shoul_6y0=np.loadtxt(resPath+path_temp+'nodout1',skiprows=1)
nod2_EEVC_shoul_6y0=np.loadtxt(resPath+path_temp+'nodout2',skiprows=1)

############################Thorax###########################################

#Ouyang2006 thorax frontal 6yo

path_temp='PIPER_Child_6YO_Validation\\Thorax\\Ouyang2006\\'
file=open(resPath+path_temp+'pp_Ouy2006.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch "binout0000";\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 M-1 x_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('genselect target node\n')
file.write('genselect node add node 35006258/0\n')
file.write('binaski newplot "binout0000" nodout 1 1 35006258 x_displacement ;\n')
file.write('xyplot 2 savefile xypair "dispN35006258" 1 all\n')
file.write('genselect target node\n')
file.write('genselect node add node 35006286/0\n')
file.write('binaski newplot "binout0000" nodout 1 1 35006286 x_displacement ;\n')
file.write('xyplot 3 savefile xypair "dispN35006286" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Ouy2006.cfile -nographics')
force_sim_Ouy6y0_thor=np.loadtxt(resPath+path_temp+'force',skiprows=1)
nod1_Ouy6y0_thor=np.loadtxt(resPath+path_temp+'dispN35006286',skiprows=1)
nod2_Ouy6y0_thor=np.loadtxt(resPath+path_temp+'dispN35006258',skiprows=1)
force_Ouy6y0_thor=np.loadtxt(resPath+path_temp+'corridor_6yo.txt')

#Kent distributed
path_temp='PIPER_Child_6YO_Validation\\Thorax\\Kent_belt_distributed\\'
file=open(resPath+path_temp+'pp_Kent_Distributed.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch "binout0000";\n')
file.write('binaski saveas ascii\n')
file.write('binaski write spcforc \n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "binout0000" nodout 1 1 35006327 x_displacement ;\n')
file.write('xyplot 1 savefile xypair "n1_x-displacement" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 35006324 x_displacement ;\n')
file.write('xyplot 1 savefile xypair "n2_x-displacement" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Kent_Distributed.cfile -nographics')
n1_Sim_Kent_Dis = np.loadtxt(resPath+path_temp+'n1_x-displacement',skiprows=1)
n2_Sim_Kent_Dis = np.loadtxt(resPath+path_temp+'n2_x-displacement',skiprows=1)
with open(resPath+path_temp+'spcforc') as f:
    force_Sim_Kent_Dis=[]
    for line in f:
        if "force resultants" in line:
            tmp=rx.findall(line)
            force_Sim_Kent_Dis.append(tmp[0])
disp_Sim_Kent_Dis=-n1_Sim_Kent_Dis;
dis_EXP_Kent_Dis_13 = np.loadtxt(resPath+path_temp+'Distributed.Kent13.displ')
forc_EXP_Kent_Dis_13 = np.loadtxt(resPath+path_temp+'Distributed.Kent13.force')
dis_EXP_Kent_Dis_29 = np.loadtxt(resPath+path_temp+'Distributed.Kent29.displ')
forc_EXP_Kent_Dis_29 = np.loadtxt(resPath+path_temp+'Distributed.Kent29.force')
dis_EXP_Kent_Dis_30 = np.loadtxt(resPath+path_temp+'Distributed.Kent30.displ')
forc_EXP_Kent_Dis_30 = np.loadtxt(resPath+path_temp+'Distributed.Kent30.force')

#Kent diagonal
path_temp='PIPER_Child_6YO_Validation\\Thorax\\Kent_belt_diagonal\\'
file=open(resPath+path_temp+'pp_Kent_Diagonal.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski saveas ascii\n')
file.write('binaski write spcforc \n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "binout0000" nodout 1 1 35006319 x_displacement ;\n')
file.write('xyplot 1 savefile xypair "n1_x-displacement" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Kent_Diagonal.cfile -nographics')

n1_Sim_Kent_Diag = np.loadtxt(resPath+path_temp+'n1_x-displacement',skiprows=1)
with open(resPath+path_temp+'spcforc') as f:
    force_Sim_Kent_Diag=[]
    for line in f:
        if "force resultants" in line:
            tmp=rx.findall(line)
            force_Sim_Kent_Diag.append(tmp[0])
disp_Sim_Kent_Diag=-n1_Sim_Kent_Diag;
dis_EXP_Kent_Diag16 = np.loadtxt(resPath+path_temp+'Diagonal.Kent16.displ')
forc_EXP_Kent_Diag16 = np.loadtxt(resPath+path_temp+'Diagonal.Kent16.force')
dis_EXP_Kent_Diag19 = np.loadtxt(resPath+path_temp+'Diagonal.Kent19.displ')
forc_EXP_Kent_Diag19 = np.loadtxt(resPath+path_temp+'Diagonal.Kent19.force')
dis_EXP_Kent_Diag33 = np.loadtxt(resPath+path_temp+'Diagonal.Kent33.displ')
forc_EXP_Kent_Diag33 = np.loadtxt(resPath+path_temp+'Diagonal.Kent33.force')


#Kent upper
path_temp='PIPER_Child_6YO_Validation\\Thorax\\Kent_upper\\'
file=open(resPath+path_temp+'pp_Kent_Upper.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski loadblock /spcforc\n')
file.write('binaski saveas ascii\n')
file.write('binaski write spcforc \n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "binout0000" nodout 1 1 35006323 x_displacement ;\n')
file.write('xyplot 1 savefile xypair "n1_x-displacement" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Kent_Upper.cfile -nographics')
n1_Sim_Kent_Upper = np.loadtxt(resPath+path_temp+'n1_x-displacement',skiprows=1)
with open(resPath+path_temp+'spcforc') as f:
    force_Sim_Kent_Upper=[]
    for line in f:
        if "force resultants" in line:
            tmp=rx.findall(line)
            force_Sim_Kent_Upper.append(tmp[0])
disp_Sim_Kent_Upper=-n1_Sim_Kent_Upper;
dis_EXP_Kent_Upper_11 = np.loadtxt(resPath+path_temp+'Upper.Kent11.displ')
forc_EXP_Kent_Upper_11 = np.loadtxt(resPath+path_temp+'Upper.Kent11.force')
dis_EXP_Kent_Upper_26 = np.loadtxt(resPath+path_temp+'Upper.Kent26.displ')
forc_EXP_Kent_Upper_26 = np.loadtxt(resPath+path_temp+'Upper.Kent26.force')
#

###############################################################################
###################### >>3 y-0 child<< ########################################
###############################################################################

#####################Pelvis####################################################

path_temp='PIPER_Child_3YO_Validation\\Pelvis\\Ouyang-2003\\'
file=open(resPath+path_temp+'pp_Ouy2003.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch "binout0000";\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 S-1 y_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "binout0000" nodout 1 1 99908809 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout1" 1 all\n')
file.write('quit\n')
file.close()

if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Ouy2003.cfile -nographics')
force_sim_ouy3yo_pel=np.loadtxt(resPath+path_temp+'force',skiprows=1)
nod1_ouy3yo_pel=np.loadtxt(resPath+path_temp+'nodout1',skiprows=1)
force_ouy3yo_pel=np.loadtxt(resPath+path_temp+'Ouyang_Subj4.txt')

##############################Shoulder#########################################

#EEVC-Q2008
path_temp='PIPER_Child_3YO_Validation\\Shoulder\\EEVC-Q2008\\'
file=open(resPath+path_temp+'pp_EEVC-Q2008.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 S-1 y_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('binaski plot "binout0000" nodout 1 1 4116934 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout1" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 20001548 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout2" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_EEVC-Q2008.cfile -nographics')
force_sim_EEVC_3yo_shoul=np.loadtxt(resPath+path_temp+'force',skiprows=1)
nod1_EEVC_3yo_shoul=np.loadtxt(resPath+path_temp+'nodout1',skiprows=1)
nod2_EEVC_3yo_shoul=np.loadtxt(resPath+path_temp+'nodout2',skiprows=1)

##############################Thorax#########################################

#Ouyang2006
path_temp='PIPER_Child_3YO_Validation\\Thorax\\Ouyang2006\\'
file=open(resPath+path_temp+'pp_Ouy2006.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch "binout0000";\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 M-1 x_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('binaski newplot "binout0000" nodout 1 1 35006258 x_displacement ;\n')
file.write('xyplot 2 savefile xypair "dispN35006258" 1 all\n')
file.write('binaski newplot "binout0000" nodout 1 1 35006286 x_displacement ;\n')
file.write('xyplot 3 savefile xypair "dispN35006286" 1 all\n')
file.write('quit\n')
file.close()
if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Ouy2006.cfile -nographics')

force_sim_Ouy_thor_3yo=np.loadtxt(resPath+path_temp+'force',skiprows=1)
nod1_Ouy_thor_3yo=np.loadtxt(resPath+path_temp+'dispN35006286',skiprows=1)
nod2_Ouy_thor_3yo=np.loadtxt(resPath+path_temp+'dispN35006258',skiprows=1)
force_Ouy_thor_3yo=np.loadtxt(resPath+path_temp+'corridor_3yo.txt')


###############################################################################
###################### >>18 m-0 child<< ########################################
###############################################################################

##############################Thorax#########################################

#Ouyang2006
path_temp='PIPER_Child_18Mo_Validation\\Thorax\\Ouyang2006\\'
file=open(resPath+path_temp+'pp_Ouy2006.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch "binout0000";\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 M-1 x_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('genselect target node\n')
file.write('genselect node add node 35006258/0\n')
file.write('binaski newplot "binout0000" nodout 1 1 35006258 x_displacement ;\n')
file.write('xyplot 2 savefile xypair "dispN35006258" 1 all\n')
file.write('genselect target node\n')
file.write('genselect node add node 35006286/0\n')
file.write('binaski newplot "binout0000" nodout 1 1 35006286 x_displacement ;\n')
file.write('xyplot 3 savefile xypair "dispN35006286" 1 all\n')
file.write('quit\n')
file.close()

if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Ouy2006.cfile -nographics')
force_sim_Ouy_18mo_thor=np.loadtxt(resPath+path_temp+'force',skiprows=1)
nod1_Ouy_18mo_thor=np.loadtxt(resPath+path_temp+'dispN35006286',skiprows=1)
nod2_Ouy_18mo_thor=np.loadtxt(resPath+path_temp+'dispN35006258',skiprows=1)
force_Ouy_18mo_thor=np.loadtxt(resPath+path_temp+'corridor_3yo.txt')


###############################################################################
########################Plots results##########################################
###############################################################################

###################### >>6 y-0 child<< ########################################


f = plt.figure(figsize=(18,12))

ax1 = f.add_subplot(341)
ax1.plot(disp_Sim_EEVC_abdo_6y0[:,1],1000*force_Sim_EEVC_abdo_6y0[:,1],'r', label='PIPER_child_model')
ax1.plot(EXP_EEVC_disp_abdo_6y0,EXP_EEVC_force_abdo_6y0,'--k', label='Experimentation')
ax1.set_title('Abdomen: frontal (EEVC, 2008)')
ax1.set_xlim([0,25])
ax1.set_ylim([0,1000])
ax1.set_xlabel('X displacement (mm)')
ax1.set_ylabel('Force (N)')
plt.legend(loc=2)

ax2 = f.add_subplot(342)
ax2.plot(teta_flexion,force_Sim_flexion,'r', label='PIPER_child_model')
ax2.plot([45,45],[147,200],'ko', label='Experimentation')
ax2.set_title('Torso flexion (part532)')
ax2.set_xlim([0,90])
ax2.set_ylim([0,1000])
ax2.set_xlabel('teta (deg)')
ax2.set_ylabel('Force (N)')
plt.legend(loc=2)

ax3 = f.add_subplot(343)
ax3.plot(disp_Sim_Kent_Lower,1000*force_Sim_Kent_Lower[:,1],'r', label='PIPER_child_model')
ax3.plot(EXP_Kent_Lower10_disp,EXP_Kent_Lower10_force,'--k', label='Exp_Kent_10')
ax3.plot(EXP_Kent_Lower25_disp,EXP_Kent_Lower25_force,'--k', label='Exp_Kent_25')
ax3.set_title('Abdomen: Kent_Lower, frontal 2011')
ax3.set_xlim([0,100])
ax3.set_ylim([0,6000])
ax3.set_xlabel('displacement (mm)')
ax3.set_ylabel('Force (N)')
plt.legend(loc='best')

ax4 = f.add_subplot(344)
ax4.plot(disp_Sim_Cham_Thigh,-1000*force_Sim_Cham_Thigh[:,1],'r', label='PIPER_child_model')
ax4.plot(EXP_Cham_Thigh_inf[:,0],10*EXP_Cham_Thigh_inf[:,1],'--k', label='Corridor_Chamouard')
ax4.plot(EXP_Cham_Thigh_sup[:,0],10*EXP_Cham_Thigh_sup[:,1],'--k')
ax4.set_title('Lex: Chamouard, ESV96')
ax4.set_xlim([0,25])
ax4.set_ylim([0,1000])
ax4.set_xlabel('X displacement (mm)')
ax4.set_ylabel('Force (N)')
plt.legend(loc='best')

ax5 = f.add_subplot(345)
ax5.set_title('Pelvis lateral impact, Ouyang 2003')
ax5.plot(-nod1_Ouy6y0_pel[:,1], force_sim_Ouy6y0_pel[:,1], 'r', label='PIPER_child_model')
ax5.plot(force_Ouy6y0_pel[:,0],force_Ouy6y0_pel[:,1],'k--', label='Exp_subj8 (Ouyang2003)')
plt.legend(fontsize=8, loc=2)
ax5.set_xlabel('impactor displ. (mm)')
ax5.set_ylabel('force (kN)')

ax6 = f.add_subplot(346)
ax6.set_title('Shoulder lateral impact, EEVC-2008')
# Force
corridorH_x=(0,4,16,35)
corridorH_y=(0.5,0.8,0.8,0.3)
corridorL_x=(0,8,26)
corridorL_y=(0,0.5,0.2)
ax6.plot(force_sim_EEVC_shoul_6y0[:,0], force_sim_EEVC_shoul_6y0[:,1], 'r', label='PIPER_child_model')
ax6.plot(corridorH_x, corridorH_y, 'k--', label='Irwin_2002_corridor')
ax6.plot(corridorL_x, corridorL_y, 'k--')
ax6.legend(fontsize=8, loc='best')
ax6.set_xlabel('time (ms)')
ax6.set_ylabel('force (kN)')

ax7 = f.add_subplot(347)
ax7.set_title('Shoulder lateral impact, EEVC-2008')
# Deflexion
corridorH_x=(0,40)
corridorH_y=(25,25)
corridorL_x=(0,40)
corridorL_y=(21,21)
ax7.plot(nod1_EEVC_shoul_6y0[:,0],nod1_EEVC_shoul_6y0[:,1]-nod2_EEVC_shoul_6y0[:,1], 'r', label='PIPER_child_model')
ax7.plot(corridorH_x, corridorH_y, 'k--', label='Irwin_2002_corridor')
ax7.plot(corridorL_x, corridorL_y, 'k--')
ax7.legend(fontsize=8, loc='best')
ax7.set_xlabel('time (ms)')
ax7.set_ylabel('deflexion (mm)')

ax8 = f.add_subplot(348)
ax8.set_title('Thorax frontal impact, Ouyang 2006')
ax8.plot(nod2_Ouy6y0_thor[:,1]-nod1_Ouy6y0_thor[:,1], force_sim_Ouy6y0_thor[:,1], 'r', label='PIPER_child_model')
ax8.plot(force_Ouy6y0_thor[:,0],force_Ouy6y0_thor[:,1],'k--', label='Ouyang_2006_corridor')
ax8.legend(fontsize=8, loc='best')
ax8.set_xlabel('deflexion (mm)')
ax8.set_ylabel('force (kN)')
ax8.set_xlim(xmin=0)

ax9 = f.add_subplot(349)
ax9.plot(disp_Sim_Kent_Dis[:,1],force_Sim_Kent_Dis,'r', label='PIPER_child_model')
ax9.plot(dis_EXP_Kent_Dis_13,1e-3*forc_EXP_Kent_Dis_13,'k', label='Kent_2011_Exp13')
ax9.plot(dis_EXP_Kent_Dis_29,1e-3*forc_EXP_Kent_Dis_29,'k--', label='Kent_2011_Exp29')
ax9.plot(dis_EXP_Kent_Dis_30,1e-3*forc_EXP_Kent_Dis_30,'k-.', label='Kent_2011_Exp30')
ax9.set_title('Trunk: Kent Distributed, frontal 2011')
ax9.set_xlim([0,40])
ax9.set_ylim([0,10])
ax9.set_xlabel('X displacement (mm)')
ax9.set_ylabel('Force (kN)')
plt.legend(loc=2)

ax10 = f.add_subplot(3,4,10)
ax10.plot(disp_Sim_Kent_Diag,force_Sim_Kent_Diag,'r', label='PIPER_child_model')
ax10.plot(dis_EXP_Kent_Diag16,0.001*forc_EXP_Kent_Diag16,'--k', label='Kent_2011_Exp16')
ax10.plot(dis_EXP_Kent_Diag19,0.001*forc_EXP_Kent_Diag19,'--k', label='Kent_2011_Exp19')
ax10.plot(dis_EXP_Kent_Diag33,0.001*forc_EXP_Kent_Diag33,'--k', label='Kent_2011_Exp33')
ax10.set_title('Thorax: Kent Diagonal, frontal 2011')
ax10.set_xlim([0,44])
ax10.set_ylim([0,6])
ax10.set_xlabel('X displacement (mm)')
ax10.set_ylabel('Force (kN)')
plt.legend(loc=2)


ax11 = f.add_subplot(3,4,11)
ax11.plot(disp_Sim_Kent_Upper[:,1],force_Sim_Kent_Upper,'r', label='PIPER_child_model')
ax11.plot(dis_EXP_Kent_Upper_11,0.001*forc_EXP_Kent_Upper_11,'--k', label='Kent_2011_Exp11')
ax11.plot(dis_EXP_Kent_Upper_26,0.001*forc_EXP_Kent_Upper_26,'--k', label='Kent_2011_Exp26')
ax11.set_title('Thorax: Kent Upper, frontal 2011')
ax11.set_xlim([0,40])
ax11.set_ylim([0,4])
ax11.set_xlabel('X displacement (mm)')
ax11.set_ylabel('Force (kN)')
plt.legend(loc=2)

plt.tight_layout()
plt.show()
f.savefig('Results_6yo.png')


###################### >>3 y-0 child<< ########################################

g = plt.figure(figsize=(18,12))

ax1=g.add_subplot(221)
ax1.plot(nod2_Ouy_thor_3yo[:,1]-nod1_Ouy_thor_3yo[:,1], force_sim_Ouy_thor_3yo[:,1], 'r', label='PIPER_child_model')
ax1.plot(force_Ouy_thor_3yo[:,0],force_Ouy_thor_3yo[:,1],'k--', label='Ouyang2006_corridor')
ax1.legend(fontsize=8, loc='best')
ax1.set_xlabel('deflexion (mm)')
ax1.set_ylabel('force (kN)')
ax1.set_title('Thorax frontal impact, Ouyang 2006')
ax1.set_xlim(xmin=0)

ax2=g.add_subplot(222)
ax2.set_title('Pelvis lateral impact, Ouyang 2003')
ax2.plot(-nod1_ouy3yo_pel[:,1], force_sim_ouy3yo_pel[:,1], 'r', label='PIPER_child_model')
ax2.plot(force_ouy3yo_pel[:,0],force_ouy3yo_pel[:,1],'k--', label='Ouyang2003_subj4')
ax2.legend(fontsize=8, loc='best')
ax2.set_xlabel('impactor displ. (mm)')
ax2.set_ylabel('force (kN)')

ax3=g.add_subplot(223)
corridorH_x=(0,4,16,36)
corridorH_y=(0.3,0.5,0.5,0.2)
corridorL_x=(0,8,26)
corridorL_y=(0,0.3,0.1)
ax3.plot(force_sim_EEVC_3yo_shoul[:,0], force_sim_EEVC_3yo_shoul[:,1], 'r', label='PIPER_child_model')
ax3.plot(corridorH_x, corridorH_y, 'k--', label='Irwin 2002 corridor')
ax3.plot(corridorL_x, corridorL_y, 'k--')
ax3.legend(fontsize=8, loc='best')
ax3.set_xlabel('time (ms)')
ax3.set_ylabel('force (kN)')
ax3.set_title('Shoulder lateral impact, EEVC-2008')

ax4=g.add_subplot(224)
corridorH_x=(0,40)
corridorH_y=(26,26)
corridorL_x=(0,40)
corridorL_y=(21,21)
ax4.plot(nod1_EEVC_3yo_shoul[:,0],nod1_EEVC_3yo_shoul[:,1]-nod2_EEVC_3yo_shoul[:,1], 'r', label='PIPER_child_model')
ax4.plot(corridorH_x, corridorH_y, 'k--', label='Irwin 2002 corridor')
ax4.plot(corridorL_x, corridorL_y, 'k--')
ax4.legend(fontsize=8, loc='best')
ax4.set_xlabel('time (ms)')
ax4.set_ylabel('def (mm)')
ax4.set_title('Shoulder lateral impact, EEVC-2008')

g.show()
plt.tight_layout()
g.savefig('Results_3yo.png')

###################### >>18 m-o child<< ########################################

h = plt.figure(figsize=(12,8))

ax1=h.add_subplot(111)
ax1.plot(nod2_Ouy_18mo_thor[:,1]-nod1_Ouy_18mo_thor[:,1], force_sim_Ouy_18mo_thor[:,1], 'r', label='PIPER_child_model')
ax1.plot(force_Ouy_18mo_thor[:,0],force_Ouy_18mo_thor[:,1],'k--', label='Ouyang2006 corridor')
ax1.legend(fontsize=8, loc='best')
ax1.set_xlabel('deflexion (mm)')
ax1.set_ylabel('force (kN)')
ax1.set_title('Thorax frontal impact, Ouyang 2006')
ax1.set_xlim(xmin=0)

h.show()
plt.tight_layout()
h.savefig('Results_18mo.png')